package grpc_client

import (
	"fmt"

	"main/config"

	"main/genproto/catalog_service"
	"main/genproto/order_service"
	"main/genproto/person_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	ClientService() person_service.ClientServiceClient
	BranchService() person_service.BranchServiceClient
	CourierService() person_service.CourierServiceClient
	UserService() person_service.UserServiceClient
	AlternativeTarifService() order_service.AlternativeTarifServiceClient
	OrderProductService() order_service.OrderProductServiceClient
	OrderService() order_service.OrderServiceClient
	DeliveryTarifService() order_service.DeliveryTarifServiceClient
	CatagoryService() catalog_service.CategoryServiceClient
	ProductService() catalog_service.ProductServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (*GrpcClient, error) {

	connOrder, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.OrderServiceHost, cfg.OrderServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("order service dial host: %s port:%d err: %s",
			cfg.OrderServiceHost, cfg.OrderServisePort, err)
	}

	connPerson, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.PersonServiceHost, cfg.PersonServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("person service dial host: %s port:%d err: %s",
			cfg.PersonServiceHost, cfg.PersonServisePort, err)
	}

	connCatalog, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.CatalogServiceHost, cfg.CatalogServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("courier service dial host: %s port:%d err: %s",
			cfg.CatalogServiceHost, cfg.CatalogServisePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{

			"category_service":       catalog_service.NewCategoryServiceClient(connCatalog),
			"product_service":        catalog_service.NewProductServiceClient(connCatalog),
			"alternative_service":    order_service.NewAlternativeTarifServiceClient(connOrder),
			"order_service":          order_service.NewOrderServiceClient(connOrder),
			"order_product_service":  order_service.NewOrderProductServiceClient(connOrder),
			"delivery_tarif_service": order_service.NewDeliveryTarifServiceClient(connOrder),
			"client_service":         person_service.NewClientServiceClient(connPerson),
			"branch_service":         person_service.NewBranchServiceClient(connPerson),
			"courier_service":        person_service.NewCourierServiceClient(connPerson),
			"user_service":           person_service.NewUserServiceClient(connPerson),
		},
	}, nil
}

func (g *GrpcClient) CatagoryService() catalog_service.CategoryServiceClient {
	return g.connections["category_service"].(catalog_service.CategoryServiceClient)
}

func (g *GrpcClient) ProductService() catalog_service.ProductServiceClient {
	return g.connections["product_service"].(catalog_service.ProductServiceClient)
}

func (g *GrpcClient) OrderProductService() order_service.OrderProductServiceClient {
	return g.connections["order_product_service"].(order_service.OrderProductServiceClient)
}

func (g *GrpcClient) OrderService() order_service.OrderServiceClient {
	return g.connections["order_service"].(order_service.OrderServiceClient)
}

func (g *GrpcClient) AlternativeTarifService() order_service.AlternativeTarifServiceClient {
	return g.connections["alternative_service"].(order_service.AlternativeTarifServiceClient)
}

func (g *GrpcClient) DeliveryTarifService() order_service.DeliveryTarifServiceClient {
	return g.connections["delivery_tarif_service"].(order_service.DeliveryTarifServiceClient)
}

func (g *GrpcClient) ClientService() person_service.ClientServiceClient {
	return g.connections["client_service"].(person_service.ClientServiceClient)
}

func (g *GrpcClient) BranchService() person_service.BranchServiceClient {
	return g.connections["branch_service"].(person_service.BranchServiceClient)
}

func (g *GrpcClient) CourierService() person_service.CourierServiceClient {
	return g.connections["courier_service"].(person_service.CourierServiceClient)
}

func (g *GrpcClient) UserService() person_service.UserServiceClient {
	return g.connections["user_service"].(person_service.UserServiceClient)
}
