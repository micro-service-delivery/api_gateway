package models

type Product struct {
	Id          string  `json:"id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Photos      string  `json:"photos"`
	OrderNumber int     `json:"order_number"`
	Type        string  `json:"type"`
	Price       float64 `json:"price"`
	CategoryId  string  `json:"category_id"`
	Active      bool    `json:"active"`
	Created_at  string  `json:"created_at"`
	Updated_at  string  `json:"updated_at"`
	Deleted_at  string  `json:"deleted_at"`
}

type CreateProduct struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Photos      string  `json:"photos"`
	OrderNumber int     `json:"order_number"`
	Type        string  `json:"type"`
	Price       float64 `json:"price"`
	CategoryId  string  `json:"category_id"`
}

type UpdateProduct struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Photos      string  `json:"photos"`
	OrderNumber int     `json:"order_number"`
	Type        string  `json:"type"`
	Price       float64 `json:"price"`
	CategoryId  string  `json:"category_id"`
	Active      bool    `json:"active"`
}
