package models

type Category struct {
	Id          string `json:"id"`
	Title       string `json:"title"`
	Image       string `json:"image"`
	OrderNumber int    `json:"order_number"`
	ParentId    string `json:"parent_id"`
	Active      bool   `json:"active"`
	Created_at  string `json:"created_at"`
	Updated_at  string `json:"updated_at"`
	Deleted_at  string `json:"deleted_at"`
}

type CreateCategory struct {
	Title       string `json:"title"`
	Image       string `json:"image"`
	OrderNumber int    `json:"order_number"`
	ParentId    string `json:"parent_id"`
}

type UpdateCategory struct {
	Title       string `json:"title"`
	Image       string `json:"image"`
	OrderNumber int    `json:"order_number"`
	ParentId    string `json:"parent_id"`
	Active      bool   `json:"active"`
}
