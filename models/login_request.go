package models

type LoginReq struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type LoginRes struct {
	Token string `json:"token"`
}
