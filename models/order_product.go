package models

type OrderProduct struct {
	Id         string  `json:"id"`
	OrderId    string  `json:"order_id"`
	ProductId  string  `json:"product_id"`
	Quantity   int     `json:"quantity"`
	Price      float64 `json:"price"`
	Created_at string  `json:"created_at"`
	Updated_at string  `json:"updated_at"`
}

type CreateOrderProduct struct {
	OrderId   string  `json:"order_id"`
	ProductId string  `json:"product_id"`
	Quantity  int     `json:"quantity"`
	Price     float64 `json:"price"`
}
