package models

type Client struct {
	Id              string  `json:"id"`
	FirstName       string  `json:"first_name"`
	LastName        string  `json:"last_name"`
	Phone           string  `json:"phone"`
	Photo           string  `json:"photo"`
	LastOrderDate   string  `json:"last_order_date"`
	BirthDate       string  `json:"birth_date"`
	DiscountType    string  `json:"discount_type"`
	TotalOrderSum   float64 `json:"total_order_sum"`
	TotalOrderCount int     `json:"total_order_count"`
	DiscountAmount  float64 `json:"discount_amount"`
	Created_at      string  `json:"created_at"`
	Updated_at      string  `json:"updated_at"`
	Deleted_at      string  `json:"deleted_at"`
}

type CreateClient struct {
	FirstName       string  `json:"first_name"`
	LastName        string  `json:"last_name"`
	Phone           string  `json:"phone"`
	Photo           string  `json:"photo"`
	LastOrderDate   string  `json:"last_order_date"`
	BirthDate       string  `json:"birth_date"`
	DiscountType    string  `json:"discount_type"`
	TotalOrderSum   float64 `json:"total_order_sum"`
	TotalOrderCount int     `json:"total_order_count"`
	DiscountAmount  float64 `json:"discount_amount"`
}
