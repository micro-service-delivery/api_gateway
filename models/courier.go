package models

type Courier struct {
	Id            string `json:"id"`
	BranchId      string `json:"branch_id"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int    `json:"max_order_count"`
	Active        bool   `json:"active"`
	Created_at    string `json:"created_at"`
	Updated_at    string `json:"updated_at"`
	Deleted_at    string `json:"deleted_at"`
}

type CreateCourier struct {
	BranchId      string `json:"branch_id"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int    `json:"max_order_count"`
}

type UpdateCourier struct {
	BranchId      string `json:"branch_id"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int    `json:"max_order_count"`
	Active        bool   `json:"active"`
}
