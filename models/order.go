package models

type Order struct {
	ID            string  `json:"id"`
	OrderId       string  `json:"order_id"`
	ClientId      string  `json:"client_id"`
	BranchId      string  `json:"branch_id"`
	CourierId     string  `json:"courier_id"`
	Type          string  `json:"type"`
	Address       string  `json:"address"`
	DeliveryPrice float64 `json:"delivery_price"`
	Price         float64 `json:"price"`
	Discount      float64 `json:"discount"`
	PaymentType   string  `json:"payment_type"`
	Status        string  `json:"status"`
	Created_at    string  `json:"created_at"`
	Updated_at    string  `json:"updated_at"`
	Deleted_at    string  `json:"deleted_at"`
}

type CreateOrder struct {
	OrderId       string  `json:"order_id"`
	ClientId      string  `json:"client_id"`
	BranchId      string  `json:"branch_id"`
	CourierId     string  `json:"courier_id"`
	Type          string  `json:"type"`
	Address       string  `json:"address"`
	DeliveryPrice float64 `json:"delivery_price"`
	Price         float64 `json:"price"`
	Discount      float64 `json:"discount"`
	PaymentType   string  `json:"payment_type"`
	Status        string  `json:"status"`
}

type OrderStatus struct {
	Id     string `json:"id"`
	Status string `json:"status"`
}
