package models

type DeliveryTarif struct {
	Id         string  `json:"id"`
	Name       string  `json:"name"`
	Type       string  `json:"type"`
	BasePrice  float64 `json:"base_price"`
	Created_at string  `json:"created_at"`
	Updated_at string  `json:"updated_at"`
}

type CreateDeliveryTarif struct {
	Name      string  `json:"name"`
	Type      string  `json:"type"`
	BasePrice float64 `json:"base_price"`
}
