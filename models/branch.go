package models

type Branch struct {
	Id              string `json:"id"`
	Name            string `json:"name"`
	UserId          string `json:"user_id"`
	Phone           string `json:"phone"`
	Photo           string `json:"photo"`
	DeliveryTarifId string `json:"delivery_tarif_id"`
	WorkStartHour   string `json:"work_start_hour"`
	WorkEndHour     string `json:"work_end_hour"`
	Destination     string `json:"destination"`
	Address         string `json:"address"`
	Active          bool   `json:"active"`
	Created_At      string `json:"created_at"`
	Updated_at      string `json:"updated_at"`
	Deleted_at      string `json:"deleted_at"`
}

type CreateBranch struct {
	Name            string `json:"name"`
	UserId          string `json:"user_id"`
	Phone           string `json:"phone"`
	Photo           string `json:"photo"`
	DeliveryTarifId string `json:"delivery_tarif_id"`
	WorkStartHour   string `json:"work_start_hour"`
	WorkEndHour     string `json:"work_end_hour"`
	Destination     string `json:"destination"`
	Address         string `json:"address"`
}

type UpdateBranch struct {
	Name            string `json:"name"`
	UserId          string `json:"user_id"`
	Phone           string `json:"phone"`
	Photo           string `json:"photo"`
	DeliveryTarifId string `json:"delivery_tarif_id"`
	WorkStartHour   string `json:"work_start_hour"`
	WorkEndHour     string `json:"work_end_hour"`
	Destination     string `json:"destination"`
	Address         string `json:"address"`
	Active          bool   `json:"active"`
}
