package models

type AlternativeTarif struct {
	Id              string  `json:"id"`
	DeliveryTarifId string  `json:"delivery_tarif_id"`
	FromPrice       float64 `json:"from_price"`
	ToPrice         float64 `json:"to_price"`
	Price           float64 `json:"price"`
	Created_at      string  `json:"created_at"`
	Updated_at      string  `json:"updated_at"`
}

type CreateAlternativeTarif struct {
	DeliveryTarifId string  `json:"delivery_tarif_id"`
	FromPrice       float64 `json:"from_price"`
	ToPrice         float64 `json:"to_price"`
	Price           float64 `json:"price"`
}
