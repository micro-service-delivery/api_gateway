package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/order_service"

	"main/models"
	"main/packages/helper"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /delivery-tarif [POST]
// @Summary      Create Tarif
// @Description  Create Tarif
// @Tags         DELIVERY_TARIF
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateDeliveryTarif  true  "Tarif data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateDeliveryTarif(c *gin.Context) {

	var tarif models.CreateDeliveryTarif
	err := c.ShouldBindJSON(&tarif)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.DeliveryTarifService().Create(c.Request.Context(), &order_service.CreateDeliveryTarif{
		Name:      tarif.Name,
		Type:      tarif.Type,
		BasePrice: float32(tarif.BasePrice),
	})

	if err != nil {
		fmt.Println("error tarif Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /delivery-tarif/{id} [put]
// @Summary      Update Tarif
// @Description  api for update tarif
// @Tags         DELIVERY_TARIF
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of tarif"
// @Param        tarif    body     models.CreateDeliveryTarif  true  "data of tarif"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateDeliveryTarif(c *gin.Context) {

	var tarif models.CreateDeliveryTarif
	err := c.ShouldBindJSON(&tarif)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.DeliveryTarifService().Update(c.Request.Context(), &order_service.DeliveryTarif{
		Id:        id,
		Name:      tarif.Name,
		Type:      tarif.Type,
		BasePrice: float32(tarif.BasePrice),
	})

	if err != nil {
		fmt.Println("error Tarif Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete tarif in cache")
	}

}

// @Router       /delivery-tarif/{id} [GET]
// @Summary      Get By Id
// @Description  get tarif by Id
// @Tags         DELIVERY_TARIF
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "tarif Id" format(uuid)
// @Success      200  {object}  models.DeliveryTarif
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetDeliveryTarif(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.DeliveryTarif{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting tarif in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.DeliveryTarifService().Get(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Tarif Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create tarif in cache")
	}

}

// @Router       /delivery-tarif [get]
// @Summary      List Tarif
// @Description  Get Tarif
// @Tags         DELIVERY_TARIF
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        name    query     string  false  "filter by name"
// @Param        type    query     string  false  "filter by type"
// @Success      200  {array}   models.DeliveryTarif
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllDeliveryTarif(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.DeliveryTarifService().GetAll(c.Request.Context(), &order_service.GetAllDeliveryTarifRequest{
		Page:  int64(page),
		Limit: int64(limit),
		Name:  c.Query("name"),
		Type:  c.Query("type"),
	})

	if err != nil {
		h.log.Error("error Tarif GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllTarif")
	c.JSON(http.StatusOK, resp)
}

// @Router       /delivery-tarif/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete tarif by id
// @Tags         DELIVERY_TARIF
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Tarif Id" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteDeliveryTarif(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error tarf GetAll:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}

	resp, err := h.grpcClient.DeliveryTarifService().Delete(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error tarif GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete tarif in cache")
	}
}
