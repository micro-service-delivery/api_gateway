package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/catalog_service"

	"main/models"
	"main/packages/helper"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /categories [POST]
// @Summary      Create Category
// @Description  Create Category
// @Tags         CATEGORY
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateCategory  true  "Category data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateCategory(c *gin.Context) {

	var category models.CreateCategory
	err := c.ShouldBindJSON(&category)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.CatagoryService().Create(c.Request.Context(), &catalog_service.CreateCategory{
		Title:       category.Title,
		Image:       category.Image,
		OrderNumber: int64(category.OrderNumber),
		ParentId:    category.ParentId,
	})

	if err != nil {
		fmt.Println("error Category Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /categories/{id} [put]
// @Summary      Update Category
// @Description  api for update category
// @Tags         CATEGORY
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of category"
// @Param        category    body     models.UpdateCategory  true  "data of category"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateCategory(c *gin.Context) {

	var category models.UpdateCategory
	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.CatagoryService().Update(c.Request.Context(), &catalog_service.Category{
		Id:          id,
		Title:       category.Title,
		Image:       category.Image,
		OrderNumber: int64(category.OrderNumber),
		ParentId:    category.ParentId,
		Active:      category.Active,
	})

	if err != nil {
		fmt.Println("error Category Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete branch in cache")
	}

}

// @Router       /categories/{id} [GET]
// @Summary      Get By Id
// @Description  get category by ID
// @Tags         CATEGORY
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Category ID" format(uuid)
// @Success      200  {object}  models.Category
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetCategory(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.Category{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting category in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.CatagoryService().Get(c.Request.Context(), &catalog_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Category Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create category in cache")
	}

}

// @Router       /categories [get]
// @Summary      List Category
// @Description  get Category
// @Tags         CATEGORY
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        title    query     string  false  "filter by name"
// @Success      200  {array}   models.Category
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllCategory(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.CatagoryService().GetAll(c.Request.Context(), &catalog_service.GetAllCategoryRequest{
		Page:  int64(page),
		Limit: int64(limit),
		Title: c.Query("title"),
	})

	if err != nil {
		h.log.Error("error Category GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllCategory")
	c.JSON(http.StatusOK, resp)
}

// @Router       /categories/{id} [DELETE]
// @Summary      DELETE BY ID
// @Description  delete category by ID
// @Tags         CATEGORY
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Category ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteCategory(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error category GetAll:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}

	resp, err := h.grpcClient.CatagoryService().Delete(c.Request.Context(), &catalog_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error category GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete category in cache")
	}
}
