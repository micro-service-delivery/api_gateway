package handler

import (
	"errors"
	"fmt"
	"math"

	"main/api/response"
	"main/genproto/order_service"
	"main/genproto/person_service"
	"main/models"
	"main/packages/helper"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /orders [POST]
// @Summary      Create Order
// @Description  Create Order
// @Tags         ORDER
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateOrder  true  "Order data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateOrder(c *gin.Context) {

	var order models.CreateOrder
	err := c.ShouldBindJSON(&order)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	// 	4. Zakazda clientga berilgan discountni hisobga olish:
	//  	- zakaz create bo'lganda clientga belgilangan discountni zakazga berish kerak

	respClient, err := h.grpcClient.ClientService().Get(c.Request.Context(), &person_service.IdReqRes{Id: order.ClientId})

	if err != nil {
		fmt.Println("error Client Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	var discountPrice float32

	if respClient.DiscountType == "fixed" {
		discountPrice = float32(respClient.DiscountAmount)
	} else if respClient.DiscountType == "percent" {
		discountPrice = float32(order.Price) * float32(respClient.DiscountAmount)
	}

	// 	10. Delivery priceni hisoblash endpiont,method:
	//  	- Branchda tanlangan Delivery Tarif bo'yicha hisoblanadi

	respBranch, err := h.grpcClient.BranchService().Get(c.Request.Context(), &person_service.IdReqRes{Id: order.BranchId})

	if err != nil {
		fmt.Println("error Branch Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	respDeliveryTarif, err := h.grpcClient.DeliveryTarifService().Get(c.Request.Context(), &order_service.IdReqRes{Id: respBranch.DeliveryTarifId})

	if err != nil {
		fmt.Println("error Delivery Tarif Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	var deliveryPrice float32

	if respDeliveryTarif.Type == "fixed" {
		deliveryPrice = respDeliveryTarif.BasePrice
	} else if respDeliveryTarif.Type == "alternative" {
		respAlternativeTarif, err := h.grpcClient.AlternativeTarifService().GetAll(c.Request.Context(), &order_service.GetAllAlternativeTarifRequest{
			Page:            1,
			Limit:           math.MaxInt,
			DeliveryTarifId: respDeliveryTarif.Id,
		})

		if err != nil {
			fmt.Println("error Alternative Tarif GetAll:", err.Error())
			c.JSON(http.StatusInternalServerError, "internal server error")
			return
		}

		for _, v := range respAlternativeTarif.AlternativeTarifs {
			if order.Price > float64(v.FromPrice) && order.Price < float64(v.ToPrice) {
				deliveryPrice = v.Price
			}
		}
	}

	resp, err := h.grpcClient.OrderService().Create(c.Request.Context(), &order_service.CreateOrder{
		OrderId:       order.OrderId,
		ClientId:      order.ClientId,
		BranchId:      order.BranchId,
		CourierId:     order.CourierId,
		Type:          order.Type,
		Address:       order.Address,
		DeliveryPrice: deliveryPrice,
		Price:         float32(order.Price),
		Discount:      discountPrice,
		PaymentType:   order.PaymentType,
		Status:        order.Status,
	})

	if err != nil {
		fmt.Println("error Order Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /orders/{id} [put]
// @Summary      Update Order
// @Description  api for update order
// @Tags         ORDER
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of order"
// @Param        order    body     models.CreateOrder  true  "data of order"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateOrder(c *gin.Context) {

	var order models.CreateOrder
	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.OrderService().Update(c.Request.Context(), &order_service.Order{
		Id:            id,
		OrderId:       order.OrderId,
		ClientId:      order.ClientId,
		BranchId:      order.BranchId,
		CourierId:     order.CourierId,
		Type:          order.Type,
		Address:       order.Address,
		DeliveryPrice: float32(order.DeliveryPrice),
		Price:         float32(order.Price),
		Discount:      float32(order.Discount),
		PaymentType:   order.PaymentType,
		Status:        order.Status,
	})

	if err != nil {
		fmt.Println("error Order Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete order in cache")
	}

}

// @Router       /orders/{id} [GET]
// @Summary      Get By Id
// @Description  get order by id
// @Tags         ORDER
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order Id" format(uuid)
// @Success      200  {object}  models.Order
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetOrder(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.Order{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting order in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.OrderService().Get(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Order Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create order in cache")
	}

}

// @Router       /orders [get]
// @Summary      List Order
// @Description  get Order
// @Tags         ORDER
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        type    query     string  false  "filter by type"
// @Param        payment_type    query     string  false  "filter by payment_type"
// @Param        branch_id    query     string  false  "filter by branch_id"
// @Param        client_id    query     string  false  "filter by client_id"
// @Param        courier_id    query     string  false  "filter by courier_id"
// @Param        order_id    query     string  false  "filter by order_id"
// @Param        from_price     query     number false "search by from_price"
// @Param        to_price     query     number false "search by to_price"
// @Success      200  {array}   models.Order
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllOrder(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	fromPrice, err := strconv.ParseFloat(c.DefaultQuery("from_price", "0"), 64)
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	toPrice, err := strconv.ParseFloat(c.DefaultQuery("to_price", fmt.Sprintf("%f", math.MaxFloat64)), 64)
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.OrderService().GetAll(c.Request.Context(), &order_service.GetAllOrderRequest{
		Page:        int64(page),
		Limit:       int64(limit),
		Type:        c.Query("type"),
		PaymentType: c.Query("payment_type"),
		OrderId:     c.Query("order_id"),
		ClientId:    c.Query("client_id"),
		CourierId:   c.Query("courier_id"),
		FromPrice:   float32(fromPrice),
		ToPrice:     float32(toPrice),
	})

	if err != nil {
		h.log.Error("error Order GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllOrder")
	c.JSON(http.StatusOK, resp)
}

// @Router       /orders/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete order by ID
// @Tags         ORDER
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order Id" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteOrder(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error Order GetAll:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}

	resp, err := h.grpcClient.OrderService().Delete(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error order GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete order in cache")
	}
}
