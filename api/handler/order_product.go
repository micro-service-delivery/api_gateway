package handler

import (
	"fmt"

	"main/api/response"
	"main/genproto/order_service"
	"main/models"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /order-products [POST]
// @Summary      Create OrderProduct
// @Description  Create OrderProduct
// @Tags         ORDER_PRODUCT
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateOrderProduct  true  "OrderProduct data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateOrderProduct(c *gin.Context) {

	var order models.CreateOrderProduct
	err := c.ShouldBindJSON(&order)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.OrderProductService().Create(c.Request.Context(), &order_service.CreateOrderProduct{
		OrderId:   order.OrderId,
		ProductId: order.ProductId,
		Quantity:  int64(order.Quantity),
		Price:     float32(order.Price),
	})

	if err != nil {
		fmt.Println("error OrderProduct Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /order-products/{id} [put]
// @Summary      Update OrderProduct
// @Description  api for update order-product
// @Tags         ORDER_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of order-product"
// @Param        order    body     models.CreateOrderProduct true  "data of order"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateOrderProduct(c *gin.Context) {

	var order models.CreateOrderProduct
	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.OrderProductService().Update(c.Request.Context(), &order_service.OrderProduct{
		Id:        id,
		OrderId:   order.OrderId,
		ProductId: order.ProductId,
		Quantity:  int64(order.Quantity),
		Price:     float32(order.Price),
	})

	if err != nil {
		fmt.Println("error OrderProduct Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete order-product in cache")
	}

}

// @Router       /order-products/{id} [GET]
// @Summary      Get By Id
// @Description  get order-product by id
// @Tags         ORDER_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "OrderProduct Id" format(uuid)
// @Success      200  {object}  models.OrderProduct
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetOrderProduct(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.OrderProduct{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting order-product in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.OrderProductService().Get(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error OrderProduct Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create order-product in cache")
	}

}

// @Router       /order-products [get]
// @Summary      List OrderProduct
// @Description  get OrderProduct
// @Tags         ORDER_PRODUCT
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Success      200  {array}   models.OrderProduct
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllOrderProduct(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.OrderProductService().GetAll(c.Request.Context(), &order_service.GetAllOrderProductRequest{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.log.Error("error OrderProduct GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllOrderProduct")
	c.JSON(http.StatusOK, resp)
}

// @Router       /order-products/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete order-product by ID
// @Tags         ORDER_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "OrderProduct Id" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteOrderProduct(c *gin.Context) {

	id := c.Param("id")

	// if !helper.IsValidUUID(id) {

	// 	h.log.Error("error OrderProduct GetAll:", logger.Error(errors.New("invalid id")))
	// 	c.JSON(http.StatusBadRequest, "invalid id")
	// 	return

	// }

	resp, err := h.grpcClient.OrderProductService().Delete(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error order-product GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete order-product in cache")
	}
}
