package handler

import (
	"fmt"

	"main/api/response"
	"main/genproto/person_service"

	"main/models"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /branches [POST]
// @Summary      Create Branch
// @Description  Create Branch
// @Tags         BRANCH
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateBranch  true  "Branch data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateBranch(c *gin.Context) {

	var branch models.CreateBranch
	err := c.ShouldBindJSON(&branch)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.BranchService().Create(c.Request.Context(), &person_service.CreateBranch{
		Name:            branch.Name,
		UserId:          branch.UserId,
		Phone:           branch.Phone,
		Photo:           branch.Photo,
		DeliveryTarifId: branch.DeliveryTarifId,
		WorkStartHour:   branch.WorkStartHour,
		WorkEndHour:     branch.WorkEndHour,
		Destination:     branch.Destination,
		Address:         branch.Address,
	})

	if err != nil {
		fmt.Println("error Branch Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /branches/{id} [put]
// @Summary      Update Branch
// @Description  api for update branch
// @Tags         BRANCH
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of branch"
// @Param        branch    body     models.UpdateBranch  true  "data of branch"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateBranch(c *gin.Context) {

	var branch models.UpdateBranch
	err := c.ShouldBindJSON(&branch)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.BranchService().Update(c.Request.Context(), &person_service.Branch{
		Id:              id,
		Name:            branch.Name,
		UserId:          branch.UserId,
		Phone:           branch.Phone,
		Photo:           branch.Photo,
		DeliveryTarifId: branch.DeliveryTarifId,
		WorkStartHour:   branch.WorkStartHour,
		WorkEndHour:     branch.WorkEndHour,
		Destination:     branch.Destination,
		Address:         branch.Address,
		Active:          branch.Active,
	})

	if err != nil {
		fmt.Println("error Branch Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete branch in cache")
	}

}

// @Router       /branches/{id} [GET]
// @Summary      Get By Id
// @Description  get branch by ID
// @Tags         BRANCH
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Branch ID" format(uuid)
// @Success      200  {object}  models.Branch
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetBranch(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.Branch{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting branch in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.BranchService().Get(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Branch Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create branch in cache")
	}

}

// @Router       /branches [get]
// @Summary      List Branch
// @Description  get Branch
// @Tags         BRANCH
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        name    query     string  false  "filter by name"
// @Param        from_date     query     string false "search by created_at_from"
// @Param        to_date     query     string false "search by created_at_to"
// @Success      200  {array}   models.Branch
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllBranch(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.BranchService().GetAll(c.Request.Context(), &person_service.GetAllBranchRequest{
		Page:     int64(page),
		Limit:    int64(limit),
		Name:     c.Query("name"),
		FromDate: c.DefaultQuery("from_date", "2000-01-01"),
		ToDate:   c.DefaultQuery("to_date", "2095-12-12"),
	})

	if err != nil {
		h.log.Error("error Branch GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllBranch")
	c.JSON(http.StatusOK, resp)
}

// @Router       /branches/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete branch by ID
// @Tags         BRANCH
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Branch ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteBranch(c *gin.Context) {

	id := c.Param("id")

	resp, err := h.grpcClient.BranchService().Delete(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error branch GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete branch in cache")
	}
}
