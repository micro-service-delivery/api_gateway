package handler

import (
	"fmt"
	"math"

	"main/api/response"
	"main/genproto/person_service"

	"main/models"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /clients [POST]
// @Summary      Create Client
// @Description  Create Client
// @Tags         CLIENT
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateClient  true  "Client data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateClient(c *gin.Context) {

	var client models.CreateClient
	err := c.ShouldBindJSON(&client)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.ClientService().Create(c.Request.Context(), &person_service.CreateClient{
		FirstName:       client.FirstName,
		LastName:        client.LastName,
		Phone:           client.Phone,
		Photo:           client.Photo,
		BirthDate:       client.BirthDate,
		LastOrderDate:   client.LastOrderDate,
		DiscountType:    client.DiscountType,
		TotalOrderSum:   float32(client.TotalOrderSum),
		TotalOrderCount: int64(client.TotalOrderCount),
		DiscountAmount:  float32(client.DiscountAmount),
	})

	if err != nil {
		fmt.Println("error Client Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /clients/{id} [put]
// @Summary      Update Client
// @Description  api for update client
// @Tags         CLIENT
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of client"
// @Param        client    body     models.CreateClient  true  "data of client"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateClient(c *gin.Context) {

	var client models.CreateClient
	err := c.ShouldBindJSON(&client)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.ClientService().Update(c.Request.Context(), &person_service.Client{
		Id:              id,
		FirstName:       client.FirstName,
		LastName:        client.LastName,
		Phone:           client.Phone,
		Photo:           client.Photo,
		BirthDate:       client.BirthDate,
		LastOrderDate:   client.LastOrderDate,
		DiscountType:    client.DiscountType,
		TotalOrderSum:   float32(client.TotalOrderSum),
		TotalOrderCount: int64(client.TotalOrderCount),
		DiscountAmount:  float32(client.DiscountAmount),
	})

	if err != nil {
		fmt.Println("error Client Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete client in cache")
	}

}

// @Router       /clients/{id} [GET]
// @Summary      Get By Id
// @Description  get client by ID
// @Tags         CLIENT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Client ID" format(uuid)
// @Success      200  {object}  models.Client
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetClient(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.Client{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting client in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.ClientService().Get(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Client Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create client in cache")
	}

}

// @Router       /clients [get]
// @Summary      List Client
// @Description  get Client
// @Tags         CLIENT
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        first_name     query     string false "search by firstName"
// @Param        last_name     query     string false "search by lastName"
// @Param        phone     query     string false "search by phone"
// @Param        discount_type     query     string false "search by discount_type"
// @Param        last_order_from_date     query     string false "search by last_order_from_date"
// @Param        last_order_to_date     query     string false "search by last_order_to_date"
// @Param        total_order_from_sum     query     number false "search by total_order_from_sum"
// @Param        total_order_to_sum     query     number false "search by total_order_to_sum"
// @Param        total_order_from_count     query     int false "search by total_order_from_count"
// @Param        total_order_to_count     query     int false "search by total_order_to_count"
// @Param        discount_from_amount     query     number false "search by discount_from_amount"
// @Param        discount_to_amount     query     number false "search by discount_to_amount"
// @Param        create_from_date     query     string false "search by created_at_from"
// @Param        create_to_date     query     string false "search by created_at_to"
// @Success      200  {array}   models.Client
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllClient(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	totalOrderFromCount, err := strconv.Atoi(c.DefaultQuery("total_order_from_count", "0"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	totalOrderToCount, err := strconv.Atoi(c.DefaultQuery("total_order_to_count", fmt.Sprintf("%d", math.MaxInt)))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	totalOrderFromSum, err := strconv.ParseFloat(c.DefaultQuery("total_order_from_sum", "0"), 64)
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	totalOrderToSum, err := strconv.ParseFloat(c.DefaultQuery("total_order_to_sum", fmt.Sprintf("%f", math.MaxFloat64)), 64)
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	discountFromAmount, err := strconv.ParseFloat(c.DefaultQuery("discount_from_amount", "0"), 64)
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	discountToAmount, err := strconv.ParseFloat(c.DefaultQuery("discount_to_amount", fmt.Sprintf("%f", math.MaxFloat64)), 64)
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.ClientService().GetAll(c.Request.Context(), &person_service.GetAllClientRequest{
		Page:                int64(page),
		Limit:               int64(limit),
		FirstName:           c.Query("first_name"),
		LastName:            c.Query("last_name"),
		Phone:               c.Query("phone"),
		DiscountType:        c.Query("discount_type"),
		TotalOrderFromCount: int64(totalOrderFromCount),
		TotalOrderToCount:   int64(totalOrderToCount),
		TotalOrderFromSum:   float32(totalOrderFromSum),
		TotalOrderToSum:     float32(totalOrderToSum),
		DiscountFromAmount:  float32(discountFromAmount),
		DiscountToAmount:    float32(discountToAmount),
	})

	if err != nil {
		h.log.Error("error Client GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllClient")
	c.JSON(http.StatusOK, resp)
}

// @Router       /clients/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete client by ID
// @Tags         CLIENT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Client ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteClient(c *gin.Context) {

	id := c.Param("id")

	resp, err := h.grpcClient.ClientService().Delete(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error client GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete client in cache")
	}
}
