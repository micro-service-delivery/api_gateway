package handler

import (
	"fmt"

	"main/api/response"
	"main/config"
	"main/genproto/person_service"
	"main/models"
	"main/packages/helper"
	"main/packages/logger"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Router       /login [post]
// @Summary      create person
// @Description  api for create persons
// @Tags         persons
// @Accept       json
// @Produce      json
// @Param        person    body     models.LoginReq  true  "data of person"
// @Success      200  {object}  models.LoginRes
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) Login(c *gin.Context) {

	// 2. Auth: login(activeligini tekshirish kerak),changePassword -> SMTP protocol

	var req models.LoginReq

	err := c.ShouldBindJSON(&req)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		res := response.ErrorResp{Code: "BAD REQUEST", Message: "invalid fields in body"}
		c.JSON(http.StatusBadRequest, res)
		return
	}

	hashPass, err := helper.GeneratePasswordHash(req.Password)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		res := response.ErrorResp{Code: "INVALID Password", Message: "invalid password"}
		c.JSON(http.StatusBadRequest, res)
		return
	}

	if req.Role == "courier" {
		resp, err := h.grpcClient.CourierService().GetCourierByUserName(c.Request.Context(), &person_service.GetByUserName{
			Login: req.Login,
		})

		if err != nil {
			fmt.Println("error Staff GetByLoging:", err.Error())
			res := response.ErrorResp{Code: "INTERNAL ERROR", Message: "internal server error"}
			c.JSON(http.StatusInternalServerError, res)
			return
		}

		err = helper.ComparePasswords([]byte(hashPass), []byte(resp.Password))

		if err != nil {
			h.log.Error("error while binding:", logger.Error(err))
			res := response.ErrorResp{Code: "INVALID Password", Message: "invalid password"}
			c.JSON(http.StatusBadRequest, res)
			return
		}

		m := make(map[string]interface{})
		m["user_id"] = resp.Id
		token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)

		if err != nil {
			return
		}

		c.JSON(http.StatusCreated, models.LoginRes{Token: token})

		return

	} else if req.Role == "user" {

		resp, err := h.grpcClient.UserService().GetUserByUserName(c.Request.Context(), &person_service.GetByUserName{
			Login: req.Login,
		})

		if err != nil {
			fmt.Println("error Staff GetByLoging:", err.Error())
			res := response.ErrorResp{Code: "INTERNAL ERROR", Message: "internal server error"}
			c.JSON(http.StatusInternalServerError, res)
			return
		}

		err = helper.ComparePasswords([]byte(hashPass), []byte(resp.Password))

		if err != nil {
			h.log.Error("error while binding:", logger.Error(err))
			res := response.ErrorResp{Code: "INVALID Password", Message: "invalid password"}
			c.JSON(http.StatusBadRequest, res)
			return
		}

		m := make(map[string]interface{})
		m["user_id"] = resp.Id
		token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)

		if err != nil {
			return
		}

		c.JSON(http.StatusCreated, models.LoginRes{Token: token})

		return

	} else {
		fmt.Println("Role undefined")
		res := response.ErrorResp{Code: "INVALID Role", Message: "invalid role"}
		c.JSON(http.StatusBadRequest, res)
		return
	}

}
