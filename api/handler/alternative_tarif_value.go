package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/order_service"

	"main/models"
	"main/packages/helper"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /alternative-tarif [POST]
// @Summary      Create Tarif
// @Description  Create Tarif
// @Tags         ALTERNATIVE_TARIF
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateAlternativeTarif  true  "Tarif data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateAlternativeTarif(c *gin.Context) {

	var tarif models.CreateAlternativeTarif
	err := c.ShouldBindJSON(&tarif)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.AlternativeTarifService().Create(c.Request.Context(), &order_service.CreateAlternativeTarif{
		DeliveryTarifId: tarif.DeliveryTarifId,
		FromPrice:       float32(tarif.FromPrice),
		ToPrice:         float32(tarif.ToPrice),
		Price:           float32(tarif.Price),
	})

	if err != nil {
		fmt.Println("error tarif Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /alternative-tarif/{id} [put]
// @Summary      Update Tarif
// @Description  api for update category
// @Tags         ALTERNATIVE_TARIF
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of tarif"
// @Param        tarif    body     models.CreateAlternativeTarif  true  "data of tarif"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateAlternativeTarif(c *gin.Context) {

	var tarif models.CreateAlternativeTarif
	err := c.ShouldBindJSON(&tarif)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.AlternativeTarifService().Update(c.Request.Context(), &order_service.AlternativeTarif{
		Id:              id,
		DeliveryTarifId: tarif.DeliveryTarifId,
		FromPrice:       float32(tarif.FromPrice),
		ToPrice:         float32(tarif.ToPrice),
		Price:           float32(tarif.Price),
	})

	if err != nil {
		fmt.Println("error Tarif Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete tarif in cache")
	}

}

// @Router       /alternative-tarif/{id} [GET]
// @Summary      Get By Id
// @Description  get tarif by Id
// @Tags         ALTERNATIVE_TARIF
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "tarif Id" format(uuid)
// @Success      200  {object}  models.AlternativeTarif
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAlternativeTarif(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.AlternativeTarif{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting tarif in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.AlternativeTarifService().Get(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Tarif Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create tarif in cache")
	}

}

// @Router       /alternative-tarif [get]
// @Summary      List Tarif
// @Description  Get Tarif
// @Tags         ALTERNATIVE_TARIF
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Success      200  {array}   models.AlternativeTarif
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllAlternativeTarif(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.AlternativeTarifService().GetAll(c.Request.Context(), &order_service.GetAllAlternativeTarifRequest{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.log.Error("error Tarif GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllTarif")
	c.JSON(http.StatusOK, resp)
}

// @Router       /alternative-tarif/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete tarif by id
// @Tags         ALTERNATIVE_TARIF
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Tarif Id" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteAlternativeTarif(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error tarf GetAll:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}

	resp, err := h.grpcClient.AlternativeTarifService().Delete(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error tarif GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete tarif in cache")
	}
}
