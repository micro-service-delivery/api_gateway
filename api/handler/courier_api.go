package handler

import (
	"fmt"
	"main/config"
	"main/genproto/order_service"
	"main/genproto/person_service"
	"main/packages/helper"
	"main/packages/logger"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /courier-api [get]
// @Summary      List Order
// @Description  get Order
// @Tags         COURIER_API
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Success      200  {array}   models.Order
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllNotCourierAcceptedOrder(c *gin.Context) {

	// 	7. Courier endpoints[get]:
	//   7.1 Qabul qilishi mumkin bo'lgan zakazlar(courier olmagan,statuslari mos kelgan)

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.OrderService().GetAllNotCourierAcceptedOrder(c.Request.Context(), &order_service.PageLimit{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.log.Error("error Order GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)
}

// @Router       /courier-api/{id} [put]
// @Summary      Update Order
// @Description  api for update order
// @Tags         COURIER_API
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of order"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteCourierInOrder(c *gin.Context) {

	// 9.Zakazdan courierni olib tashlash uchun endpoint:
	//  - zakazni statusi 'Accepted'ga o'zgaradi

	id := c.Param("id")

	respOrder, err := h.grpcClient.OrderService().Get(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		fmt.Println("error Order Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	resp, err := h.grpcClient.OrderService().Update(c.Request.Context(), &order_service.Order{
		Id:            id,
		OrderId:       respOrder.OrderId,
		ClientId:      respOrder.ClientId,
		BranchId:      respOrder.BranchId,
		CourierId:     "",
		Type:          respOrder.Type,
		Address:       respOrder.Address,
		DeliveryPrice: float32(respOrder.DeliveryPrice),
		Price:         float32(respOrder.Price),
		Discount:      float32(respOrder.Discount),
		PaymentType:   respOrder.PaymentType,
		Status:        "accepted",
	})

	if err != nil {
		fmt.Println("error Order Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete order in cache")
	}

}

// @Router       /courier-api/{id} [put]
// @Summary      Update Order
// @Description  api for update order
// @Tags         COURIER_API
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of order"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CourierGetOrder(c *gin.Context) {

	// 	8. Courier zakazni olishi uchun endpiont(API):
	//  - zakazda courier bor bo'lsa error qaytarish,
	//  - courierda max orders countga teng zakazlari bo'lsa error qaytarish
	//  - zakaz statusi 'Courier Accepted'ga o'zgaradi

	tokenString := c.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	respCourier, err := h.grpcClient.CourierService().Get(c.Request.Context(), &person_service.IdReqRes{Id: claims.UserID})

	if err != nil {
		fmt.Println("error Order Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	id := c.Param("id")

	respOrder, err := h.grpcClient.OrderService().Get(c.Request.Context(), &order_service.IdReqRes{Id: id})

	if err != nil {
		fmt.Println("error Order Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	//  - zakazda courier bor bo'lsa error qaytarish
	if respOrder.CourierId != "" {
		fmt.Println("error this order already received")
		c.JSON(http.StatusBadRequest, "order already received")
		return
	}

	respCouriersOrder, err := h.grpcClient.OrderService().GetAllByCourierId(c.Request.Context(), &order_service.CourierIdRequest{
		CourierId: respOrder.CourierId})

	if err != nil {
		fmt.Println("error Order Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	//  - courierda max orders countga teng zakazlari bo'lsa error qaytarish
	if respCouriersOrder.Count == respCourier.MaxOrderCount {
		fmt.Println("error you can`t get some order")
		c.JSON(http.StatusBadRequest, "can`t get some order")
		return
	}

	//  - zakaz statusi 'Courier Accepted'ga o'zgaradi
	resp, err := h.grpcClient.OrderService().Update(c.Request.Context(), &order_service.Order{
		Id:            id,
		OrderId:       respOrder.OrderId,
		ClientId:      respOrder.ClientId,
		BranchId:      respOrder.BranchId,
		CourierId:     claims.UserID,
		Type:          respOrder.Type,
		Address:       respOrder.Address,
		DeliveryPrice: float32(respOrder.DeliveryPrice),
		Price:         float32(respOrder.Price),
		Discount:      float32(respOrder.Discount),
		PaymentType:   respOrder.PaymentType,
		Status:        "courier_accepted",
	})

	if err != nil {
		fmt.Println("error Order Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete order in cache")
	}

}
