package handler

import (
	"fmt"

	"main/api/response"
	"main/genproto/person_service"

	"main/models"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /users [POST]
// @Summary      Create User
// @Description  Create User
// @Tags         USER
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateUser  true  "User data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateUser(c *gin.Context) {

	var user models.CreateUser
	err := c.ShouldBindJSON(&user)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.UserService().Create(c.Request.Context(), &person_service.CreateUser{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Phone:     user.Phone,
		Login:     user.Login,
		Password:  user.Password,
	})

	if err != nil {
		fmt.Println("error User Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /users/{id} [put]
// @Summary      Update User
// @Description  api for update user
// @Tags         USER
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of user"
// @Param        user    body     models.UpdateUser  true  "data of user"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateUser(c *gin.Context) {

	var user models.UpdateUser
	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.UserService().Update(c.Request.Context(), &person_service.User{
		Id:        id,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Phone:     user.Phone,
		Login:     user.Login,
		Password:  user.Password,
		Active:    user.Active,
	})

	if err != nil {
		fmt.Println("error User Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete user in cache")
	}

}

// @Router       /users/{id} [GET]
// @Summary      Get By Id
// @Description  get user by ID
// @Tags         USER
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "User ID" format(uuid)
// @Success      200  {object}  models.User
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetUser(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.User{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting user in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.UserService().Get(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error User Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while User client in cache")
	}

}

// @Router       /users [get]
// @Summary      List User
// @Description  get User
// @Tags         USER
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        first_name     query     string false "search by firstName"
// @Param        last_name     query     string false "search by lastName"
// @Param        phone     query     string false "search by phone"
// @Param        from_date     query     string false "search by created_at_from"
// @Param        to_date     query     string false "search by created_at_to"
// @Success      200  {array}   models.User
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllUser(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.UserService().GetAll(c.Request.Context(), &person_service.GetAllUserRequest{
		Page:      int64(page),
		Limit:     int64(limit),
		FirstName: c.Query("first_name"),
		LastName:  c.Query("last_name"),
		Phone:     c.Query("phone"),
		FromDate:  c.DefaultQuery("from_date", "2000-01-01"),
		ToDate:    c.DefaultQuery("to_date", "2095-12-12"),
	})

	if err != nil {
		h.log.Error("error User GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllUser")
	c.JSON(http.StatusOK, resp)
}

// @Router       /users/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete user by ID
// @Tags         USER
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "User ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteUser(c *gin.Context) {

	id := c.Param("id")

	resp, err := h.grpcClient.UserService().Delete(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error user GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete user in cache")
	}
}
