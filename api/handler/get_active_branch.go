package handler

import (
	"fmt"
	"main/genproto/person_service"
	"main/packages/logger"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// @Router       /get-branches [get]
// @Summary      List Branch
// @Description  get Branch
// @Tags         GET_BRANCH
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Success      200  {array}   models.Branch
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllActiveBranch(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	// 3. Branchlarni active va hozir ish vaqti to'g'ri keladiganlarini get qilish uchun endpoint(API)

	timeNow := time.Now().Format("15:04:05")

	fmt.Println(timeNow)

	resp, err := h.grpcClient.BranchService().GetAllActive(c.Request.Context(), &person_service.GetAllActiveBranchRequest{
		Page:  int64(page),
		Limit: int64(limit),
		Date:  timeNow,
	})

	if err != nil {
		h.log.Error("error Branch Active GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllBranch")
	c.JSON(http.StatusOK, resp)
}
