package handler

import (
	"fmt"
	"main/genproto/order_service"
	"main/genproto/person_service"
	"main/models"
	"main/packages/logger"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Router       /order-status/{id} [put]
// @Summary      Update Order
// @Description  api for update order status
// @Tags         ORDER_STATUS
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of order"
// @Param   	 status  query     string     true  "status enums"       Enums(accepted, courier_accepted, ready_in_branch, on_way, finished, canceled)
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateOrderStatus(c *gin.Context) {

	var order models.OrderStatus
	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	// 5. Zakaz statusini o'zgartirish uchun endpoint(API)

	resp, err := h.grpcClient.OrderService().Update(c.Request.Context(), &order_service.Order{
		Id:     id,
		Status: order.Status,
	})

	if err != nil {
		fmt.Println("error Order Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	// 6. Zakaz zavershit bo'lganda clientni ma'lumotlari(last_ordered_date,total_orders_count,total_orders_sum)ni update qilish

	if order.Status == "finished" {
		respOrder, err := h.grpcClient.OrderService().Get(c.Request.Context(), &order_service.IdReqRes{Id: order.Id})

		if err != nil {
			fmt.Println("error Order Get:", err.Error())
			c.JSON(http.StatusInternalServerError, "internal server error")
			return
		}

		respClient, err := h.grpcClient.ClientService().Get(c.Request.Context(), &person_service.IdReqRes{Id: respOrder.ClientId})

		if err != nil {
			fmt.Println("error Client Get:", err.Error())
			c.JSON(http.StatusInternalServerError, "internal server error")
			return
		}

		_, err = h.grpcClient.ClientService().Update(c.Request.Context(), &person_service.Client{
			Id:              respOrder.ClientId,
			FirstName:       respClient.FirstName,
			LastName:        respClient.LastName,
			Phone:           respClient.Phone,
			Photo:           respClient.Photo,
			BirthDate:       respClient.BirthDate,
			DiscountType:    respClient.DiscountType,
			DiscountAmount:  respClient.DiscountAmount,
			LastOrderDate:   respOrder.CreatedAt,
			TotalOrderCount: respClient.TotalOrderCount + 1,
			TotalOrderSum:   respClient.TotalOrderSum + respOrder.Price,
		})

		if err != nil {
			fmt.Println("error Client Update:", err.Error())
			c.JSON(http.StatusInternalServerError, "internal server error")
			return
		}
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete order in cache")
	}

}
