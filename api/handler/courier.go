package handler

import (
	"fmt"

	"main/api/response"
	"main/genproto/person_service"

	"main/models"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /couriers [POST]
// @Summary      Create Courier
// @Description  Create Courier
// @Tags         COURIER
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateCourier  true  "Courier data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateCourier(c *gin.Context) {

	var courier models.CreateCourier
	err := c.ShouldBindJSON(&courier)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.CourierService().Create(c.Request.Context(), &person_service.CreateCourier{
		BranchId:      courier.BranchId,
		FirstName:     courier.FirstName,
		LastName:      courier.LastName,
		Phone:         courier.Phone,
		Login:         courier.Login,
		Password:      courier.Password,
		MaxOrderCount: int64(courier.MaxOrderCount),
	})

	if err != nil {
		fmt.Println("error Courier Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /couriers/{id} [put]
// @Summary      Update Courier
// @Description  api for update courier
// @Tags         COURIER
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of courier"
// @Param        courier    body     models.UpdateCourier  true  "data of courier"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateCourier(c *gin.Context) {

	var courier models.UpdateCourier
	err := c.ShouldBindJSON(&courier)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.CourierService().Update(c.Request.Context(), &person_service.Courier{
		Id:            id,
		BranchId:      courier.BranchId,
		FirstName:     courier.FirstName,
		LastName:      courier.LastName,
		Phone:         courier.Phone,
		Login:         courier.Login,
		Password:      courier.Password,
		MaxOrderCount: int64(courier.MaxOrderCount),
		Active:        courier.Active,
	})

	if err != nil {
		fmt.Println("error Courier Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete courier in cache")
	}

}

// @Router       /couriers/{id} [GET]
// @Summary      Get By Id
// @Description  get courier by ID
// @Tags         COURIER
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Courier ID" format(uuid)
// @Success      200  {object}  models.Courier
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetCourier(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.Courier{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting courier in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.CourierService().Get(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Courier Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Courier client in cache")
	}

}

// @Router       /couriers [get]
// @Summary      List Courier
// @Description  get Courier
// @Tags         COURIER
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        first_name     query     string false "search by firstName"
// @Param        last_name     query     string false "search by lastName"
// @Param        phone     query     string false "search by phone"
// @Param        from_date     query     string false "search by created_at_from"
// @Param        to_date     query     string false "search by created_at_to"
// @Success      200  {array}   models.Courier
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllCourier(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.CourierService().GetAll(c.Request.Context(), &person_service.GetAllCourierRequest{
		Page:      int64(page),
		Limit:     int64(limit),
		FirstName: c.Query("first_name"),
		LastName:  c.Query("last_name"),
		Phone:     c.Query("phone"),
		FromDate:  c.DefaultQuery("from_date", "2000-01-01"),
		ToDate:    c.DefaultQuery("to_date", "2095-12-12"),
	})

	if err != nil {
		h.log.Error("error Courier GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllCourier")
	c.JSON(http.StatusOK, resp)
}

// @Router       /couriers/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete courier by ID
// @Tags         COURIER
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Courier ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteCourier(c *gin.Context) {

	id := c.Param("id")

	resp, err := h.grpcClient.CourierService().Delete(c.Request.Context(), &person_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error courier GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete courier in cache")
	}
}
