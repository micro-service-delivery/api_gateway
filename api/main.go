package api

import (
	_ "main/api/docs"
	"main/api/handler"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func NewServer(h *handler.Handler) *gin.Engine {
	r := gin.Default()

	r.POST("/categories", h.CreateCategory)
	r.GET("/categories", h.GetAllCategory)
	r.GET("/categories/:id", h.GetCategory)
	r.PUT("/categories/:id", h.UpdateCategory)
	r.DELETE("/categories/:id", h.DeleteCategory)

	r.POST("/products", h.CreateProduct)
	r.GET("/products", h.GetAllProduct)
	r.GET("/products/:id", h.GetProduct)
	r.PUT("/products/:id", h.UpdateProduct)
	r.DELETE("/products/:id", h.DeleteProduct)

	r.POST("/orders", h.CreateOrder)
	r.GET("/orders", h.GetAllOrder)
	r.GET("/orders/:id", h.GetOrder)
	r.PUT("/orders/:id", h.UpdateOrder)
	r.DELETE("/orders/:id", h.DeleteOrder)

	r.POST("/order-products", h.CreateOrderProduct)
	r.GET("/order-products", h.GetAllOrderProduct)
	r.GET("/order-products/:id", h.GetOrderProduct)
	r.PUT("/order-products/:id", h.UpdateOrderProduct)
	r.DELETE("/order-products/:id", h.DeleteOrderProduct)

	r.POST("/delivery-tarif", h.CreateDeliveryTarif)
	r.GET("/delivery-tarif", h.GetAllDeliveryTarif)
	r.GET("/delivery-tarif/:id", h.GetDeliveryTarif)
	r.PUT("/delivery-tarif/:id", h.UpdateDeliveryTarif)
	r.DELETE("/delivery-tarif/:id", h.DeleteDeliveryTarif)

	r.POST("/alternative-tarif", h.CreateAlternativeTarif)
	r.GET("/alternative-tarif", h.GetAllAlternativeTarif)
	r.GET("/alternative-tarif/:id", h.GetAlternativeTarif)
	r.PUT("/alternative-tarif/:id", h.UpdateAlternativeTarif)
	r.DELETE("/alternative-tarif/:id", h.DeleteAlternativeTarif)

	r.POST("/branches", h.CreateBranch)
	r.GET("/branches", h.GetAllBranch)
	r.GET("/branches/:id", h.GetBranch)
	r.PUT("/branches/:id", h.UpdateBranch)
	r.DELETE("/branches/:id", h.DeleteBranch)

	r.POST("/clients", h.CreateClient)
	r.GET("/clients", h.GetAllClient)
	r.GET("/clients/:id", h.GetClient)
	r.PUT("/clients/:id", h.UpdateClient)
	r.DELETE("/clients/:id", h.DeleteClient)

	r.POST("/couriers", h.CreateCourier)
	r.GET("/couriers", h.GetAllCourier)
	r.GET("/couriers/:id", h.GetCourier)
	r.PUT("/couriers/:id", h.UpdateCourier)
	r.DELETE("/couriers/:id", h.DeleteCourier)

	r.POST("/users", h.CreateUser)
	r.GET("/users", h.GetAllUser)
	r.GET("/users/:id", h.GetUser)
	r.PUT("/users/:id", h.UpdateUser)
	r.DELETE("/users/:id", h.DeleteUser)

	r.GET("/get-branches", h.GetAllActiveBranch)

	r.GET("/order-status", h.UpdateOrderStatus)

	r.GET("/courier-api", h.GetAllNotCourierAcceptedOrder)
	r.PUT("/courier-api/:id", h.DeleteCourierInOrder)
	r.PUT("/courier-api/:id", h.CourierGetOrder)

	r.POST("/login", h.Login)

	// r.PUT("/change-password/:id", h.ChangePassword)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
